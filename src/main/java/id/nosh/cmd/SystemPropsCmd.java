/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.cmd;

import java.util.Optional;
import java.util.Properties;

import id.nosh.ext.Command;
import id.nosh.ext.Description;
import id.nosh.ext.DisplayName;
import id.nosh.ext.MainMethod;

/**
 * @author indroneel
 *
 */

@Command({"sysprop"})
@Description("Prints the system property information for the current JVM.")
public class SystemPropsCmd {

	@MainMethod
	@Description("Type 'sysprop' to get a list of all available system properties. "
			+ "Type 'sysprop [key]' to get the value of the property [key].")
	public void execute(@DisplayName("key") Optional<String> key) {

		if(key.isPresent()) {
			printProp(key.get());
		}
		else {
			printAllProps();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void printProp(String propName) {
		Properties sysProps = System.getProperties();
		if(sysProps.containsKey(propName)) {
			System.out.println(sysProps.getProperty(propName));
		}
		else {
			System.out.printf("%s: This property is not available\n", propName);
		}
	}

	private void printAllProps() {
		Properties sysProps = System.getProperties();
		int maxWidth = 0;
		for(String propName : sysProps.stringPropertyNames()) {
			maxWidth = Math.max(maxWidth, propName.length());
		}

		String fmt = "%-" + maxWidth + "s : %s\n";
		System.out.println("These system properties are currently available with this JVM:\n");
		for(String propName : sysProps.stringPropertyNames()) {
			System.out.printf(fmt, propName, sysProps.getProperty(propName));
		}
	}
}
