/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.cmd;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import id.crossbinder.hod.Inject;
import id.nosh.ext.Command;
import id.nosh.ext.CommandOption;
import id.nosh.ext.Description;
import id.nosh.ext.DisplayName;
import id.nosh.ext.Environment;
import id.nosh.ext.GroupBy;
import id.nosh.ext.MainMethod;
import id.nosh.ext.Mandatory;

/**
 * @author indroneel
 *
 */

@Command({"env"})
@Description("Modifies or prints the set of environment variables that are currently available.")
public class EnvironmentCmd {

	@Inject
	private Environment env;

	private enum OpType {
		STORE, PUT, REMOVE, LIST
	}

	private OpType  optype;
	private String  keyName;
	private boolean replace;

	@CommandOption(simple = "s", full = "store")
	@Description("Stores the key-value pair provided to disk")
	@GroupBy("operation")
	@Mandatory
	public void setStoreOp(String name) {
		optype = OpType.STORE;
		keyName = name;
	}

	@CommandOption(simple = "p", full = "put")
	@Description("Adds the key-value pair but does not save to disk")
	@GroupBy("operation")
	@Mandatory
	public void setPutOp(String name) {
		optype = OpType.PUT;
		keyName = name;
	}

	@CommandOption(simple = "l", full = "list")
	@Description("Shows all key-value pairs, or a specific one by name")
	@GroupBy("operation")
	@Mandatory
	public void setListOp(Optional<String> name) {
		optype = OpType.LIST;
		if(name.isPresent()) {
			keyName = name.get();
		}
	}

	@CommandOption(simple = "d", full = "delete")
	@Description("Removes a key and all associated values")
	@GroupBy("operation")
	@Mandatory
	public void setDeleteOp(String name) {
		optype = OpType.REMOVE;
		keyName = name;
	}

	@CommandOption(simple = "r", full = "replace")
	@Description("In case of put or store, replaces all existing values with the new one "
			+ "for this key")
	public void setReplace() {
		replace = true;
	}

	@MainMethod
	@Description("[name] is the variable name to modify or delete with the corresponding "
			+ "[value]. No value needs to be provided in case of delete.")
	public void execute(@DisplayName("value") Optional<String> value) {

		if(OpType.STORE.equals(optype)) {
			if(!value.isPresent()) {
				System.out.println("environment not set. Missing value information");
				return;
			}
			env.store(keyName, value.get(), replace);
		}

		if(OpType.PUT.equals(optype)) {
			if(!value.isPresent()) {
				System.out.println("environment not set. Missing value information");
				return;
			}
			env.set(keyName, value.get(), replace);
		}

		if(OpType.REMOVE.equals(optype)) {
			env.remove(keyName);
		}

		if(OpType.LIST.equals(optype)) {
			if(keyName == null) {
				listAll();
			}
			else {
				listOne(keyName);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void listAll() {
		if(env.listNames().isEmpty()) {
			System.out.println("No environment variables");
			return;
		}

		int maxWidth = 0;
		for(String key : env.listNames()) {
			maxWidth = Math.max(maxWidth, key.length());
		}

		String fmt = "%-" + maxWidth + "s : %s\n";
		System.out.println("Environment variables available:\n");

		for(String key : env.listNames()) {
			String displayVal = "";
			Collection<String> values = env.getAll(key);
			if(values.isEmpty()) {
				displayVal = "";
			}
			else if(values.size() == 1) {
				displayVal = values.iterator().next();
			}
			else {
				displayVal = values.toString();
			}
			System.out.printf(fmt, key, displayVal);
		}
	}

	private void listOne(String key) {
		List<String> values = env.getAll(key);
		if(values == null || values.isEmpty()) {
			System.out.printf("%s - environment variable not set", key);
			return;
		}
		System.out.println(values);
	}
}
