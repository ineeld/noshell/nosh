/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.cmd;

import id.nosh.ext.Command;
import id.nosh.ext.Description;
import id.nosh.ext.MainMethod;

/**
 * @author indroneel
 *
 */

@Command({"exit", "quit"})
@Description("Comes out of interactive mode and ends this program")
public class ExitCmd {

	@MainMethod
	public void execute() {
		System.out.println("goodbye!!");
		System.out.println();
		System.exit(0);
	}
}
