/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.cmd;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import id.crossbinder.hod.Inject;
import id.nosh.ext.Command;
import id.nosh.ext.CommandContext;
import id.nosh.ext.CommandDetails;
import id.nosh.ext.Description;
import id.nosh.ext.DisplayName;
import id.nosh.ext.MainMethod;

/**
 * @author indroneel
 *
 */

@Command({"help"})
@Description("Prints detailed imformation on how to execute a given command.")

public class HelpCmd {

	private static final int TEXT_WIDTH = 80;

	@Inject
	private CommandContext cmdCtxt;

	@MainMethod
	@Description("Type 'help' to get a list of all available commands. Type 'help [cmd]' to get more "
			+ "information about the command [cmd].")

	public void execute(@DisplayName("cmd") Optional<String> cmdName) {
		if(cmdName.isPresent()) {
			printHelp(cmdName.get());
		}
		else {
			printAllHelp();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void printAllHelp() {
		List<NameDesc> ndlist = new ArrayList<>();
		List<String> cmdNames = cmdCtxt.listNames();

		for(String cnam : cmdNames) {
			NameDesc nd = new NameDesc();
			nd.name = cnam;
			CommandDetails cdet = cmdCtxt.getDetails(cnam);
			nd.desc = cdet.getDescription();
			if(nd.desc == null || nd.desc.trim().length() == 0) {
				nd.desc = "no description available";
			}
			ndlist.add(nd);
		}
		Collections.sort(ndlist);

		int maxwidth = 0;
		for(NameDesc nd : ndlist) {
			maxwidth = Math.max(maxwidth, nd.name.length());
		}
		String fmtStr = "%-" + maxwidth + "s  %s\n";

		System.out.println("These commands are currently available.  Type 'help' to see this list.");
		System.out.println();

		for(NameDesc nd : ndlist) {
			System.out.printf(fmtStr, nd.name, nd.desc);
		}

		System.out.println("\nType 'help <cmd>' to find out more about the command <cmd>.");
	}

	private void printHelp(String cmdName) {
		CommandDetails cmdet = cmdCtxt.getDetails(cmdName);
		if(cmdet == null) {
			System.out.println("cannot help. command does not exist: " + cmdName);
			return;
		}

		printCmdDetails(cmdName, cmdet);
		printCmdUsage(cmdName, cmdet);
		printParamDesc(cmdet);
		printCmdOptions(cmdet.getOptions());
	}

	private void printCmdDetails(String cmdName, CommandDetails cmdet) {
		StringBuilder sb = new StringBuilder();
		sb.append(cmdName).append(" - ");
		String desc = cmdet.getDescription();
		if(desc != null) {
			sb.append(desc);
		}
		sb.append('\n');

		PrintWriter writer = new PrintWriter(System.out, true);
		HelpFormatter hf = new HelpFormatter();
		hf.printWrapped(writer, TEXT_WIDTH, sb.toString());
	}

	private void printCmdUsage(String cmdName, CommandDetails cmdet) {
		System.out.println("\nUSAGE:\n");
		StringWriter sw = new StringWriter();
		HelpFormatter hf = new HelpFormatter();

		Options opts = cmdet.getOptions();
		if(opts == null || opts.getOptions().isEmpty()) {
			sw.write("  " + cmdName + " ");
		}
		else {
			hf.setSyntaxPrefix("  ");
			hf.printUsage(new PrintWriter(sw, true), TEXT_WIDTH, cmdName, opts);
		}

		if(cmdet.getParameterUsage() != null) {
			sw.write(cmdet.getParameterUsage());
		}

		hf.printWrapped(new PrintWriter(System.out, true),
			TEXT_WIDTH,
			sw.toString().replace('\n', ' '));
	}

	private void printParamDesc(CommandDetails cmdet) {
		StringBuilder sb = new StringBuilder();

		String desc = cmdet.getParameterDescription();
		if(desc != null && desc.trim().length() > 0) {
			sb.append('\n').append('\n').append(desc).append('\n');
		}

		PrintWriter writer = new PrintWriter(System.out, true);
		HelpFormatter hf = new HelpFormatter();
		hf.printWrapped(writer, TEXT_WIDTH, sb.toString());
	}

	private void printCmdOptions(Options opts) {
		if(opts == null || opts.getOptions().isEmpty()) {
			return;
		}

		System.out.println("\nOPTIONS:\n");

		PrintWriter writer = new PrintWriter(System.out, true);
		HelpFormatter hf = new HelpFormatter();
		hf.printOptions(writer, TEXT_WIDTH, opts,
			HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Name to Desc mapping class

	private class NameDesc implements Comparable<NameDesc> {
		String name;
		String desc;

		@Override
		public int compareTo(NameDesc nd) {
			if(nd == null) {
				return 1;
			}
			if(nd.name == null) {
				return 1;
			}
			return name.compareTo(nd.name);
		}
	}
}
