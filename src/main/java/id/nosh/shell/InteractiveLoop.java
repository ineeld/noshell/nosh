/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.shell;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import id.nosh.ext.CommandContext;
import id.nosh.ext.ExecutionError;

/**
 * @author indroneel
 *
 */

public class InteractiveLoop implements Runnable {

	private static final Logger _L = Logger.getLogger(InteractiveLoop.class.getName());

	private String bannerPath;
	private String prompt;
	private CommandContext cmdCtxt;

	void setBanner(String path) {
		bannerPath = path;
	}

	void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	void setCommandContext(CommandContext cc) {
		cmdCtxt = cc;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface Runnable

	@Override
	public void run() {
		if(bannerPath != null) {
			printBanner();
		}

		if(prompt == null) {
			prompt = "$ ";
		}
		BufferedReader sinrdr = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			System.out.print(prompt);
			System.out.flush();

			List<String> args = null;
			try {
				String cmdLine = sinrdr.readLine();
				args = makeArgs(cmdLine);
			}
			catch(Exception exep) {
				_L.log(Level.WARNING, exep.getMessage(), exep);
				System.out.println("\nunable to parse command line\n");
				break;
			}

			if(args.isEmpty()) {
				continue;
			}

			String cmdName = args.get(0); //we keep this only for console messaging purposes.
			try {
				System.out.println(); // throw in a blank line.
				cmdCtxt.execute(args);
			}
			catch(ExecutionError err) {
				_L.log(Level.WARNING, err.getType().name(), err);
				switch(err.getType()) {
					case COMMAND_NOT_FOUND:
						System.out.printf("command not found: '%s'\n", err.getMessage());
						System.out.println("type 'help' for get a list of available commands\n");
						break;
					case BAD_COMMAND_LINE:
						System.out.printf("%s: %s\n", cmdName, err.getCause().getMessage());
						System.out.printf("try 'help %s' for usage and additional information\n",
							cmdName);
						break;
					case TOO_FEW_ARGUMENTS:
						System.out.printf("%s: Too few arguments provided\n", cmdName);
						System.out.printf("try 'help %s' for usage and additional information\n",
							cmdName);
						break;
					case BAD_ARGUMENT:
						System.out.printf("%s: Invalid value provided for argument %s\n",
							cmdName, err.getCause().getMessage());
						System.out.printf("try 'help %s' for usage and additional information\n",
							cmdName);
						break;
					case COMMAND_CREATION_FAILED:
						System.out.printf("%s: command creation failed.\n", cmdName);
						System.out.println("Check log messages for more details");
						break;
					case COMMAND_EXECUTION_FAILED:
						System.out.printf("command %s terminated abnormally.\n", cmdName);
						System.out.printf("Error is: %s", err.getCause());
						break;
					case UNSPECIFIED:
					default:
						System.out.println("Unknown error!! Check log messages for more details");
				}

			}
			catch(Throwable th) {
				System.out.println("nosh has encountered an internal error.");
				System.out.println("More details are available in log messages (if enabled)");
			}
			System.out.println();
		}
	}

	private List<String> makeArgs(String cmdLine) throws IOException {
		StreamTokenizer tok = new StreamTokenizer(new StringReader(cmdLine));
		tok.resetSyntax();
		tok.quoteChar('"');
		tok.wordChars('#', '~');
		tok.whitespaceChars(' ', ' ');
		ArrayList<String> args = new ArrayList<>();
		while(tok.nextToken() != StreamTokenizer.TT_EOF) {
			args.add(tok.sval);
		}
		return args;
	}

	private void printBanner() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if(cl == null) {
			cl = getClass().getClassLoader();
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		InputStream ips = null;
		try {
			ips = cl.getResourceAsStream(bannerPath);
			int oc;
			while((oc = ips.read()) >= 0) {
				bos.write(oc);
				byte[] chunk = new byte[1024];
				int amt = ips.read(chunk);
				bos.write(chunk, 0, amt);
			}
			bos.flush();
			bos.close();
			bos.writeTo(System.out);
		}
		catch(Exception exep) {
			_L.log(Level.FINEST, "unable to load banner", exep);
		}
		finally {
			try {
				ips.close();
			}
			catch (Exception exep) {
				_L.log(Level.FINEST, "unable to close banner resource", exep);
			}
		}
	}
}
