/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.shell;

import id.crossbinder.hod.Crossbinder;
import id.nosh.ext.CommandContext;
import id.scanpath.ScanScope;

/**
 * @author indroneel
 *
 */

public class Nosh {

	private ScanScope       cmdScope;
	private InteractiveLoop loop;

	public Nosh() {
		loop = new InteractiveLoop();
	}

	public Nosh scanCommand(ScanScope scope) {
		cmdScope = scope;
		cmdScope.includePackage("id.nosh.cmd");
		return this;
	}

	public Nosh setBanner(String path) {
		loop.setBanner(path);
		return this;
	}

	public Nosh setPrompt(String prompt) {
		loop.setPrompt(prompt);
		return this;
	}

	public void start() {
		ScanScope xbs = new ScanScope()
				.includePackage("id.nosh.shell")
				.includePackage("id.nosh.entities");
		Crossbinder xb = Crossbinder
			.create()
			.scanClasses(xbs);
		xb.start();

		if(cmdScope == null) {
			cmdScope = new ScanScope().includePackage("id.nosh.cmd");
		}

		CommandContext cmdCtxt = xb.locator().get(CommandContext.class);
		cmdCtxt.prepare(cmdScope);
		loop.setCommandContext(cmdCtxt);

		Thread lthrd = new Thread(loop, "interactive-loop");
		lthrd.start();
	}
}
