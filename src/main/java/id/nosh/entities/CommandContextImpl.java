/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import id.crossbinder.hod.Injector;
import id.crossbinder.hod.InjectorAware;
import id.crossbinder.hod.Singleton;
import id.nosh.ext.Command;
import id.nosh.ext.CommandContext;
import id.nosh.ext.CommandDetails;
import id.nosh.ext.ExecutionError;
import id.scanpath.ClasspathScanner;
import id.scanpath.ScanScope;

/**
 * @author indroneel
 *
 */

@Singleton
public class CommandContextImpl implements InjectorAware, CommandContext {

	private static final Logger _L = Logger.getLogger(CommandContextImpl.class.getName());

	private Map<String, CommandDetailsImpl> cmdMap;
	private List<String> nameList;
	private Injector     injector;

	public CommandContextImpl() {
		cmdMap = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface InjectorAware

	@Override
	public void setInjector(Injector injector) {
		this.injector = injector;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface CommandContext

	@Override
	public void prepare(ScanScope scope) {
		cmdMap.clear();
		ClasspathScanner scanner = new ClasspathScanner();
		scanner.load(scope);
		int nameLenMax = 0; //for debugging purposes
		List<Class<?>> clsList = scanner.listAnnotatedClasses(Command.class);
		for(Class<?> cls : clsList) {
			CommandDetailsImpl cdet = new CommandDetailsImpl(cls);
			if(cdet.validateAndLoad()) {
				for(String name : cdet.getNames()) {
					cmdMap.put(name, cdet);
					nameLenMax = Math.max(nameLenMax, name.length());
				}
			}
		}

		//debug dump start
		String fmtStr = "%-" + nameLenMax + "s  %s";
		_L.info("---- Command Map ----------------------------");
		for(String name : cmdMap.keySet()) {
			_L.info(String.format(fmtStr, name, cmdMap.get(name).getCommandType().getName()));
		}
		_L.info("---------------------------------------------");
		//debug dump end

		List<String> list = new ArrayList<>(cmdMap.keySet());
		nameList = Collections.unmodifiableList(list);
	}

	@Override
	public List<String> listNames() {
		return nameList;
	}

	@Override
	public CommandDetails getDetails(String name) {
		return cmdMap.get(name);
	}

	@Override
	public void execute(List<String> args) throws ExecutionError {
		String cmdName = args.remove(0);
		CommandDetailsImpl cdet = cmdMap.get(cmdName);
		if(cdet == null) {
			throw new ExecutionError(ExecutionError.ErrorType.COMMAND_NOT_FOUND, cmdName);
		}

		CommandExecutor exec = new CommandExecutor(cdet, injector);
		exec.execute(args.toArray(new String[args.size()]));
	}
}
