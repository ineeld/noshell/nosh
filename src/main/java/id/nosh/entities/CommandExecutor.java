/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import id.crossbinder.hod.Injector;
import id.nosh.ext.ExecutionError;
import id.nosh.ext.ExecutionError.ErrorType;

/**
 * @author indroneel
 *
 */

public class CommandExecutor {

	private static final Logger _L = Logger.getLogger(CommandExecutor.class.getName());

	private CommandDetailsImpl cdet;
	private Injector           injector;

	CommandExecutor(CommandDetailsImpl cd, Injector injector) {
		cdet = cd;
		this.injector = injector;
		_L.fine(String.format("executing [%s]", cdet.getCommandType().getName()));
	}

	public void execute(String[] args) throws ExecutionError {
		DefaultParser parser = new DefaultParser();

		CommandLine cmdLine = null;
		try {
			cmdLine = parser.parse(cdet.getOptions(), args);
		}
		catch (ParseException exep) {
			throw new ExecutionError(ExecutionError.ErrorType.BAD_COMMAND_LINE, exep);
		}

		Object cmd = null;
		try {
			cmd = cdet.getCommandType().getDeclaredConstructor().newInstance();
		}
		catch (InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException exep) {
			throw new ExecutionError(ExecutionError.ErrorType.COMMAND_CREATION_FAILED, exep);
		}

		injector.inject(cmd);

		Option[] options = cmdLine.getOptions();
		if(options != null && options.length > 0) {
			for(Option opt : options) {
				Method optMthd = cdet.getOptionMethod(opt.getOpt());
				if(optMthd == null) {
					throw new ExecutionError(ErrorType.UNSPECIFIED);
				}
				_L.fine(String.format("executing option method: %s", optMthd.getName()));
				String[] optVals = opt.getValues();
				if(optVals == null) {
					optVals = new String[0];
				}
				OptionMethodParams omp = new OptionMethodParams(optMthd.getParameters());
				Object[] paramVals = omp.createInvocationValues(optVals);
				try {
					optMthd.invoke(cmd, paramVals);
				}
				catch (IllegalAccessException
						| IllegalArgumentException | InvocationTargetException exep) {
					throw new ExecutionError(
							ExecutionError.ErrorType.COMMAND_EXECUTION_FAILED, exep);
				}
			}
		}

		Method mainMthd = cdet.getMainMethod();
		String[] mainVals = cmdLine.getArgs();
		if(mainVals == null) {
			mainVals = new String[0];
		}
		MainMethodParams mmp = new MainMethodParams(mainMthd.getParameters());
		Object[] paramVals = mmp.createInvocationValues(mainVals);

		try {
			mainMthd.invoke(cmd, paramVals);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException exep) {
			throw new ExecutionError(ExecutionError.ErrorType.COMMAND_EXECUTION_FAILED, exep);
		}
	}
}
