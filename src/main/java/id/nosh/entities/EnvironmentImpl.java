/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import id.crossbinder.hod.Initializable;
import id.crossbinder.hod.Singleton;
import id.nosh.ext.Environment;

/**
 * @author indroneel
 *
 */

@Singleton
public class EnvironmentImpl implements Environment, Initializable {

	private static final Logger _L = Logger.getLogger(EnvironmentImpl.class.getName());

	private static final String STORE_FILE_NAME = "config/environment";

	private MultiValuedMap<String, String> localMap;
	private MultiValuedMap<String, String> storedMap;

	public EnvironmentImpl() {
		localMap = new ArrayListValuedHashMap<>();
		storedMap = new ArrayListValuedHashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface Environment

	@Override
	public List<String> listNames() {
		LinkedList<String> names = new LinkedList<String>();
		names.addAll(localMap.keySet());
		names.addAll(storedMap.keySet());
		return names;
	}

	@Override
	public void set(String name, String value, boolean replace) {
		if(replace) {
			storedMap.remove(name);
			localMap.remove(name);
			localMap.put(name, value);
		}
		else {
			Collection<String> oldVals = storedMap.remove(name);
			if(oldVals != null) {
				for(String val : oldVals) {
					localMap.put(name, val);
				}
			}
			localMap.put(name, value);
		}
		saveToDisk();
	}

	@Override
	public void store(String name, String value, boolean replace) {
		if(replace) {
			localMap.remove(name);
			storedMap.remove(name);
			storedMap.put(name, value);
		}
		else {
			Collection<String> oldVals = localMap.remove(name);
			if(oldVals != null) {
				for(String val : oldVals) {
					storedMap.put(name, val);
				}
			}
			storedMap.put(name, value);
		}
		saveToDisk();
	}

	@Override
	public String get(String name) {
		Collection<String> values = null;
		if(localMap.containsKey(name)) {
			values = localMap.get(name);
		}
		else if(storedMap.containsKey(name)) {
			values = storedMap.get(name);
		}

		if(values == null || values.isEmpty()) {
			return null;
		}
		return values.iterator().next();
	}

	@Override
	public List<String> getAll(String name) {
		LinkedList<String> result = new LinkedList<>();
		if(localMap.containsKey(name)) {
			result.addAll(localMap.get(name));
		}
		else if(storedMap.containsKey(name)) {
			result.addAll(storedMap.get(name));
		}
		return result;
	}

	@Override
	public void remove(String name) {
		if(localMap.containsKey(name)) {
			localMap.remove(name);
		}
		else if(storedMap.containsKey(name)) {
			storedMap.remove(name);
			saveToDisk();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface Initializable

	@Override
	public void initialize() {

		FileReader fr = null;
		try {
			fr = new FileReader(STORE_FILE_NAME);
			BufferedReader reader = new BufferedReader(fr);
			String oneLine = null;
			while((oneLine = reader.readLine()) != null) {
				String[] parts = oneLine.split(":", 2);
				if(parts.length != 2) {
					continue;
				}
				String key = parts[0].trim();
				String value = parts[1].trim();
				storedMap.put(key, value);
			}
			reader.close();
		}
		catch(Exception exep) {
			_L.log(Level.WARNING, "failed to load environment data", exep);
		}
		finally {
			try {
				if(fr != null) {
					fr.close();
				}
			}
			catch(Exception exep) {
				_L.log(Level.WARNING, "error closing environment data file", exep);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Methods

	private void saveToDisk() {

		FileWriter fw = null;

		try {
			fw = new FileWriter(STORE_FILE_NAME);
			PrintWriter pw = new PrintWriter(fw);
			for(String key : storedMap.keySet()) {
				Collection<String> values = storedMap.get(key);
				for(String value : values) {
					pw.format("%s : %s\n", key, value);
				}
				pw.flush();
			}
			pw.close();
		}
		catch(Exception exep) {
			_L.log(Level.WARNING, "failed to store environment data", exep);
		}
		finally {
			try {
				if(fw != null) {
					fw.close();
				}
			}
			catch(Exception exep) {
				_L.log(Level.WARNING, "error closing environment data file", exep);
			}
		}
	}
}
