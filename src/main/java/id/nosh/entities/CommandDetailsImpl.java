/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

import id.nosh.ext.Command;
import id.nosh.ext.CommandDetails;
import id.nosh.ext.Description;
import id.nosh.ext.MainMethod;

/**
 * @author indroneel
 *
 */

class CommandDetailsImpl implements CommandDetails {

	private static final Logger _L = Logger.getLogger(CommandDetailsImpl.class.getName());

	private List<String>        names;
	private Map<String, Method> optMthdMap;

	private Class<?> cmdCls;
	private Method   mainMthd;
	private String   description;
	private String   paramDesc;
	private String   paramUsage;


	CommandDetailsImpl(Class<?> cls) {
		this.cmdCls = cls;
		names = new ArrayList<>();
		optMthdMap = new HashMap<>();
	}

	boolean validateAndLoad() {
		names.clear();
		Command cmdAnn = cmdCls.getAnnotation(Command.class);
		if(cmdAnn == null || cmdAnn.value() == null) {
			return false;
		}
		names.addAll(Arrays.asList(cmdAnn.value()));
		if(names.isEmpty()) {
			return false;
		}

		Description descAnn = cmdCls.getAnnotation(Description.class);
		if(descAnn != null && descAnn.value() != null) {
			description = descAnn.value().trim();
		}

		mainMthd = resolveMainMethod();
		if(mainMthd == null) {
			return false;
		}

		MainMethodParams mmp = new MainMethodParams(mainMthd.getParameters());
		paramUsage = mmp.generateUsage();

		descAnn = mainMthd.getAnnotation(Description.class);
		if(descAnn != null && descAnn.value() != null) {
			paramDesc = descAnn.value().trim();
		}

		optMthdMap.clear();
		optMthdMap.putAll(new CommandOptionsBuilder().buildOptionMethodMap(cmdCls));

		return true;
	}

	List<String> getNames() {
		return names;
	}

	Class<?> getCommandType() {
		return cmdCls;
	}

	Method getMainMethod() {
		return mainMthd;
	}

	Method getOptionMethod(String shortForm) {
		return optMthdMap.get(shortForm);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of interface CommandDetails

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getParameterDescription() {
		return paramDesc;
	}

	@Override
	public String getParameterUsage() {
		return paramUsage;
	}

	@Override
	public Options getOptions() {
		return new CommandOptionsBuilder().build(cmdCls);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	public Method resolveMainMethod() {
		Method mainMthd = null;
		Method[] methods = cmdCls.getMethods();
		int mmcount = 0;
		for(Method mthd : methods) {
			if(isMainMethod(mthd)) {
				_L.fine("found main method: " + mthd.toGenericString());
				mmcount++;
				mainMthd = mthd;
			}
		}
		if(mmcount == 1) {
			return mainMthd;
		}
		else {
			_L.warning(String.format("Too many main methods in %s. Found %d",
					cmdCls.getName(), mmcount));
			return null;
		}
	}

	private boolean isMainMethod(Method mthd) {
		if(mthd.getAnnotation(MainMethod.class) == null) {
			return false;
		}

		//basic checks: method must be public and not be abstract or static.
		int mod = mthd.getModifiers();
		if(Modifier.isAbstract(mod) || Modifier.isStatic(mod) || !Modifier.isPublic(mod)) {
			return false;
		}

		//basic checks: method return type must be void.
		if(!mthd.getReturnType().equals(Void.TYPE)) {
			return false;
		}

		MainMethodParams mmp = new MainMethodParams(mthd.getParameters());
		if(!mmp.validate()) {
			_L.fine(String.format("parameters not allowed: %s", mthd.toGenericString()));
			return false;
		}
		return true;
	}
}
