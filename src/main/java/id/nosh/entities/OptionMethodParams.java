/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.awt.List;
import java.lang.reflect.Array;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.cli.Option;

import id.nosh.ext.DisplayName;
import id.nosh.ext.ExecutionError;
import id.nosh.ext.ExecutionError.ErrorType;

/**
 * @author indroneel
 *
 */

public class OptionMethodParams {

	private static final Logger _L = Logger.getLogger(OptionMethodParams.class.getName());

	private Parameter[] mthdParams;

	public OptionMethodParams(Parameter[] params) {
		mthdParams = params;
		if(mthdParams == null) {
			mthdParams = new Parameter[0];
		}
	}

	public boolean validate() {
		if(mthdParams.length == 0) {
			// if there are no parameters, things should check out to be good.
			return true;
		}

		LinkedList<Parameter> plist = new LinkedList<>(Arrays.asList(mthdParams));

		Parameter firstParam = plist.removeFirst();
		Class<?> firstType = firstParam.getType();
		if(plist.isEmpty()) {
			//method has only one argument.
			if(firstType.isArray()) {
				Class<?> subType = firstType.getComponentType();
				return isAllowed(subType);
			}

			if(List.class.isAssignableFrom(firstType) || Set.class.isAssignableFrom(firstType)
					|| Optional.class.isAssignableFrom(firstType)) {
				ParameterizedType tt = (ParameterizedType) firstParam.getParameterizedType();
				if(tt.getActualTypeArguments() != null && tt.getActualTypeArguments().length == 1) {
					Type ttt = tt.getActualTypeArguments()[0];
					if(ttt instanceof Class) {
						return isAllowed((Class<?>) ttt);
					}
				}
				return false;
			}

			return isAllowed(firstType);
		}

		//else
		while(!plist.isEmpty()) {
			Parameter param = plist.removeFirst();
			if(!isAllowed(param.getType())) {
				return false;
			}
		}
		return true;
	}

	public boolean isOptionalArg() {
		if(mthdParams.length != 1) {
			return false;
		}

		Class<?> ptype = mthdParams[0].getType();
		if(ptype.isArray()) {
			return true;
		}
		if(List.class.isAssignableFrom(ptype) || Set.class.isAssignableFrom(ptype)
				|| Optional.class.isAssignableFrom(ptype)) {
			return true;
		}
		return false;
	}

	public int getArgCount() {
		if(mthdParams.length == 1) {
			Class<?> ptype = mthdParams[0].getType();
			if(ptype.isArray()
				|| List.class.isAssignableFrom(ptype) || Set.class.isAssignableFrom(ptype)
				|| Optional.class.isAssignableFrom(ptype)) {
				return Option.UNLIMITED_VALUES;
			}
		}
		return mthdParams.length;
	}

	public String getDisplayName(char separator) {
		StringBuilder sb = new StringBuilder();

		boolean first = true;
		for(Parameter param : mthdParams) {
			if(!first) {
				sb.append(separator);
			}

			String pname = param.getName();
			DisplayName dnAnn = param.getAnnotation(DisplayName.class);
			if(dnAnn != null && dnAnn.value() != null && dnAnn.value().trim().length() > 0) {
				pname = dnAnn.value().trim();
			}

			Class<?> ptype = param.getType();
			if(ptype.isArray() || List.class.isAssignableFrom(ptype)
					|| Set.class.isAssignableFrom(ptype)) {
				sb.append(pname).append('1')
				.append(separator)
				.append('[').append(pname).append('N').append(']');
			}
			else if(Optional.class.isAssignableFrom(ptype)) {
				sb.append('[').append(pname).append(']');
			}
			else {
				sb.append(pname);
			}
			first = false;
		}
		return sb.toString();
	}

	public Object[] createInvocationValues(String[] args) throws ExecutionError {
		if(mthdParams.length == 0) {
			return new Object[0];
		}

		LinkedList<Parameter> plist = new LinkedList<>(Arrays.asList(mthdParams));
		LinkedList<String> argList = new LinkedList<>(Arrays.asList(args));

		_L.fine(String.format("received option values: %s", argList));

		LinkedList<Object> paramVals = new LinkedList<>();

		//handle the first parameter specially for arrays, lists, set or optional value.
		Parameter param = plist.removeFirst();
		Class<?> ptype = param.getType();

		boolean processMore = true;
		if(ptype.isArray()) {
			Class<?> subtype = ptype.getComponentType();
			Object[] pval = (Object[]) Array.newInstance(subtype, 0);
			if(!argList.isEmpty()) {
				pval = (Object[]) Array.newInstance(subtype, argList.size());
				for(int i=0; i<argList.size(); i++) {
					pval[i] = convertOne(subtype, argList.get(i));
					if(pval[i] == null) {
						throw new ExecutionError(ErrorType.BAD_ARGUMENT);
					}
				}
				argList.clear();
			}
			paramVals.add(pval);
			processMore = false;
		}
		else if(List.class.isAssignableFrom(ptype)) {
			LinkedList<Object> lstVal = new LinkedList<>();
			if(!argList.isEmpty()) {
				Class<?> subtype = inferSubType(param);
				if(subtype == null) {
					throw new ExecutionError(ErrorType.UNSPECIFIED); //This should not happen
				}
				for(int i=0; i<argList.size(); i++) {
					Object elem = convertOne(subtype, argList.get(i));
					if(elem == null) {
						throw new ExecutionError(ErrorType.BAD_ARGUMENT);
					}
					lstVal.add(elem);
				}
				argList.clear();
			}
			paramVals.add(lstVal);
			processMore = false;
		}
		else if(Set.class.isAssignableFrom(ptype)) {
			HashSet<Object> setVal = new HashSet<>();
			if(!argList.isEmpty()) {
				Class<?> subtype = inferSubType(param);
				if(subtype == null) {
					throw new ExecutionError(ErrorType.UNSPECIFIED); //This should not happen
				}
				for(int i=0; i<argList.size(); i++) {
					Object elem = convertOne(subtype, argList.get(i));
					if(elem == null) {
						throw new ExecutionError(ErrorType.BAD_ARGUMENT);
					}
					setVal.add(elem);
				}
				argList.clear();
			}
			paramVals.add(setVal);
			processMore = false;
		}
		else if(Optional.class.isAssignableFrom(ptype)) {
			Optional<Object> optVal = Optional.empty();
			if(!argList.isEmpty()) {
				Class<?> subtype = inferSubType(param);
				if(subtype == null) {
					throw new ExecutionError(ErrorType.UNSPECIFIED);
				}
				Object pval = convertOne(subtype, argList.removeFirst());
				optVal = Optional.ofNullable(pval);
			}
			paramVals.add(optVal);
			processMore = false;
		}
		else {
			if(argList.isEmpty()) {
				throw new ExecutionError(ExecutionError.ErrorType.TOO_FEW_ARGUMENTS);
			}
			Object pval = convertOne(ptype, argList.removeFirst());
			if(pval == null) {
				throw new ExecutionError(
					ExecutionError.ErrorType.BAD_ARGUMENT, param.getName());
			}
			paramVals.add(pval);
			processMore = true;
		}

		while(!plist.isEmpty() && processMore) {
			param = plist.removeFirst();
			ptype = param.getType();

			System.out.println(ptype);
			if(argList.isEmpty()) {
				throw new ExecutionError(ExecutionError.ErrorType.TOO_FEW_ARGUMENTS);
			}
			Object pval = convertOne(ptype, argList.removeFirst());
			if(pval == null) {
				throw new ExecutionError(
					ExecutionError.ErrorType.BAD_ARGUMENT, param.getName());
			}
			paramVals.add(pval);
		}

		_L.fine(String.format("parameter values: %s", paramVals.toString()));
		if(!plist.isEmpty()) {
			throw new ExecutionError(); //TODO: set error type
		}
		return paramVals.toArray(new Object[paramVals.size()]);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private boolean isAllowed(Class<?> ptype) {
		if(ptype.equals(Boolean.TYPE) || ptype.equals(Boolean.class)) {
			return true;
		}
		if(ptype.equals(Byte.TYPE) || ptype.equals(Byte.class)) {
			return true;
		}
		if(ptype.equals(Character.TYPE) || ptype.equals(Character.class)) {
			return true;
		}
		if(ptype.equals(Short.TYPE) || ptype.equals(Short.class)) {
			return true;
		}
		if(ptype.equals(Integer.TYPE) || ptype.equals(Integer.class)) {
			return true;
		}
		if(ptype.equals(Long.TYPE) || ptype.equals(Long.class)) {
			return true;
		}
		if(ptype.equals(Float.TYPE) || ptype.equals(Float.class)) {
			return true;
		}
		if(ptype.equals(Double.TYPE) || ptype.equals(Double.class)) {
			return true;
		}
		if(ptype.equals(String.class)) {
			return true;
		}
		return false;
	}

	private Object convertOne(Class<?> ptype, String arg) {

		_L.fine(String.format("converting arg: %s to type: %s", arg, ptype.getName()));

		if(ptype.equals(Boolean.TYPE) || ptype.equals(Boolean.class)) {
			return Boolean.valueOf(arg);
		}
		if(ptype.equals(Byte.TYPE) || ptype.equals(Byte.class)) {
			return Byte.valueOf(arg);
		}
		if(ptype.equals(Character.TYPE) || ptype.equals(Character.class)) {
			if(arg.length() > 0) {
				return arg.charAt(0);
			}
			return null;
		}
		if(ptype.equals(Short.TYPE) || ptype.equals(Short.class)) {
			return Short.valueOf(arg);
		}
		if(ptype.equals(Integer.TYPE) || ptype.equals(Integer.class)) {
			return Integer.valueOf(arg);
		}
		if(ptype.equals(Long.TYPE) || ptype.equals(Long.class)) {
			return Long.valueOf(arg);
		}
		if(ptype.equals(Float.TYPE) || ptype.equals(Float.class)) {
			return Float.valueOf(arg);
		}
		if(ptype.equals(Double.TYPE) || ptype.equals(Double.class)) {
			return Double.valueOf(arg);
		}
		if(ptype.equals(String.class)) {
			return arg;
		}
		return null;
	}

	private Class<?> inferSubType(Parameter param) {
		ParameterizedType tt = (ParameterizedType) param.getParameterizedType();
		if(tt.getActualTypeArguments() != null && tt.getActualTypeArguments().length > 0) {
			Type ttt = tt.getActualTypeArguments()[0];
			if(ttt instanceof Class) {
				return (Class<?>) ttt;
			}
			else {
				return null;
			}
		}
		return null;
	}
}
