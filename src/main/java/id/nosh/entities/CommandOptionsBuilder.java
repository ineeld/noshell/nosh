/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.entities;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

import id.nosh.ext.CommandOption;
import id.nosh.ext.Description;
import id.nosh.ext.GroupBy;
import id.nosh.ext.Mandatory;
import id.nosh.ext.ValueSeparator;

/**
 * @author indroneel
 *
 */

class CommandOptionsBuilder {

	//private static final Logger _L = Logger.getLogger(CommandOptionsBuilder.class.getName());

	public Options build(Class<?> cls) {
		HashMap<String, OptionGroup> groupMap = new HashMap<>();
		Options opts = new Options();
		Method[] methods = cls.getMethods();
		for(Method mthd : methods) {
			Option opt = buildOption(mthd, groupMap);
			if(opt != null) {
				opts.addOption(opt);
			}
		}
		for(OptionGroup og : groupMap.values()) {
			opts.addOptionGroup(og);
		}
		return opts;
	}

	public Map<String, Method> buildOptionMethodMap(Class<?> cls) {
		HashMap<String, Method> optMthdMap = new HashMap<>();
		Method[] methods = cls.getMethods();
		for(Method mthd : methods) {
			String shortForm = validateToShortForm(mthd);
			if(shortForm != null) {
				optMthdMap.put(shortForm, mthd);
			}
		}
		return optMthdMap;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private Option buildOption(Method mthd, Map<String, OptionGroup> groupMap) {

		String shortForm = validateToShortForm(mthd);
		if(shortForm == null) {
			return null;
		}

		Option.Builder builder = Option.builder(shortForm);

		CommandOption optAnn = mthd.getAnnotation(CommandOption.class);
		Parameter[] mthdParams = mthd.getParameters();
		OptionMethodParams omp = new OptionMethodParams(mthdParams);

		String longForm = optAnn.full();
		if(longForm != null) {
			longForm = longForm.trim();
			if(longForm.length() > 0) {
				builder.longOpt(longForm);
			}
		}

		Mandatory mandAnn = mthd.getAnnotation(Mandatory.class);
		if(mandAnn != null) {
			builder.required();
		}

		Description descAnn = mthd.getAnnotation(Description.class);
		if(descAnn != null) {
			String desc = descAnn.value();
			if(desc != null && desc.trim().length() > 0) {
				builder.desc(desc.trim());
			}
		}

		builder.optionalArg(omp.isOptionalArg());

		if(omp.getArgCount() == 1) {
			builder.hasArg();
			builder.optionalArg(omp.isOptionalArg());
		}
		else if(omp.getArgCount() > 1) {
			builder.numberOfArgs(omp.getArgCount());
		}

		char separator = ' ';
		ValueSeparator vsAnn = mthd.getAnnotation(ValueSeparator.class);
		if(vsAnn != null && vsAnn.value() != null && vsAnn.value().trim().length() == 1) {
			separator = vsAnn.value().trim().charAt(0);
			builder.valueSeparator(separator);
		}

		String argName = omp.getDisplayName(separator);
		builder.argName(argName);

		Option opt = builder.build();

		GroupBy grpAnn = mthd.getAnnotation(GroupBy.class);
		if(grpAnn != null && grpAnn.value() != null) {
			String grpName = grpAnn.value().trim();
			if(grpName.length() > 0) {
				OptionGroup group = groupMap.get(grpName);
				if(group == null) {
					group = new OptionGroup();
					groupMap.put(grpName, group);
				}
				if(opt.isRequired()) {
					group.setRequired(true);
				}
				group.addOption(opt);
				return null;
			}
		}
		return opt;
	}

/**
 * Evaluates if a method meets all the pre-requisites for being an option method and return the
 * short form of the option.
 * <p>
 *
 * @param	mthd the method being considered.
 * @return	the short option or null if the method cannot be used to populate an option.
 */

	private String validateToShortForm(Method mthd) {
		//basic checks: method must be public and not be abstract or static.
		int mod = mthd.getModifiers();
		if(Modifier.isAbstract(mod) || Modifier.isStatic(mod) || !Modifier.isPublic(mod)) {
			return null;
		}

		//basic checks: method return type must be void.
		if(!mthd.getReturnType().equals(Void.TYPE)) {
			return null;
		}

		CommandOption optAnn = mthd.getAnnotation(CommandOption.class);
		if(optAnn == null) {
			return null;
		}

		Parameter[] mthdParams = mthd.getParameters();
		OptionMethodParams omp = new OptionMethodParams(mthdParams);

		if(!omp.validate()) {
			return null;
		}

		String shortForm = optAnn.simple();
		if(shortForm == null) {
			return null;
		}
		if(shortForm.length() != 1) {
			return null;
		}
		return shortForm;
	}
}
